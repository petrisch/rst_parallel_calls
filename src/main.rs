extern crate csv;

use chrono::format::ParseError;
use chrono::{Duration, NaiveDateTime, NaiveTime};
use serde::Deserialize;
use std::error::Error;
use std::fmt;
use std::io;

// The structure of the import is something like: "28.01.2022 16:29:29,00:01:35"
// so first there is some time, and then some duration
#[derive(Deserialize)]
struct Import {
    time: String,
    duration: String,
}

// At the end we only want to know how many calls are simultaneously,
// we store it together with the basic timestamp we calculated it together
// (optional was helpfull for debugging)
struct CallResult {
    first_appearence: NaiveDateTime,
    number_of_calls: i32,
}

// Both structs implement Display also for debugging
impl fmt::Display for Import {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "time: {}, duration: {}",
            self.time.to_string(),
            self.duration.to_string()
        )
    }
}

impl fmt::Display for CallResult {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "First Appearence: {}, Number of calls: {}",
            self.first_appearence.to_string(),
            self.number_of_calls.to_string()
        )
    }
}

// Read the csv from stin and give it back
// Usage as "cargo run < data.csv"
fn read_csv() -> Result<Vec<Import>, Box<dyn Error>> {
    // Use a comma as delimiter
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(b',')
        .from_reader(io::stdin());

    let mut import: Vec<Import> = Vec::new();

    for result in rdr.deserialize() {
        // Push every column of "time" onto a vector
        // TODO: This needs a first column to have names,
        // which is not always the case and wasn't the initial task
        let time: Import = result?;
        import.push(time);
    }
    // TODO: Handle the error and not just panic
    return Ok(import);
}

// Process a string to a datetime
fn string_to_naivedatetime(timestring: &str) -> Result<NaiveDateTime, ParseError> {
    let local = NaiveDateTime::parse_from_str(timestring, "%d.%m.%Y %H:%M:%S")?;
    return Ok(local);
}

// Process a string to a duration
// which is a bit tedious in rust, since there is no direct parse_from_string for duration
fn string_to_duration(durationstring: &str) -> Result<Duration, ParseError> {
    let tz = NaiveTime::from_hms(0, 0, 0);
    let local = NaiveTime::parse_from_str(durationstring, "%H:%M:%S")?;

    let duration: Duration = local.signed_duration_since(tz);

    return Ok(duration);
}

// Determine if two timestamps with theyr duration overlap in time
fn determine_overlap(
    datetime1: &NaiveDateTime,
    duration1: Duration,
    datetime2: &NaiveDateTime,
    duration2: Duration,
) -> bool {
    // Calculate the difference of the possibly later in time timestamp, but could be both.
    let difference = datetime2.signed_duration_since(*datetime1);
    let zero = Duration::zero();

    // If the difference is positiv, then datetime2 must be after datetime1,
    // So if this difference is less than the first call lasted, then we have a simultaneous call
    if difference > zero {
        if difference < duration1 {
            true
        } else {
            false
        }
    }
    // If its the opposite, then the first datetime was after the second, which means
    // the difference must be compared to the second duration, and that one must be bigger for a hit
    else {
        if -difference < duration2 {
            true
        } else {
            false
        }
    }
}

// Starting point of the actual program

fn main() {
    let csv = read_csv();

    // Create a new Vector where we will store found overlaps later
    let mut c_result: Vec<CallResult> = Vec::new();

    // Comparison of the result of the csv read. the _ is because we don't actually use the
    // result, but the content of it which is in csv
    let _import = match csv {
        // If the import succeeded
        Ok(csv) => {
            // Compare every entry in the csv and use the index from the first argument
            // to store hits later at this position
            for (s_position, stamp) in csv.iter().enumerate() {
                println!("comparing timestamp: {}", stamp);

                // Create a Result on every timestamp, so that the vector has this entry, otherwise its null
                let call_result = CallResult {
                    first_appearence: string_to_naivedatetime(&stamp.time).unwrap(),
                    number_of_calls: 0,
                };
                c_result.push(call_result);

                // Compare every timestamp that follows the one we are starting with
                // TODO: This is a naive approach, maybe there is a better one.
                for runner in csv.iter().skip(s_position + 1) {
                    // Determine if they overlap
                    let overlap = determine_overlap(
                        &string_to_naivedatetime(&stamp.time).unwrap(),
                        string_to_duration(&stamp.duration).unwrap(),
                        &string_to_naivedatetime(&runner.time).unwrap(),
                        string_to_duration(&runner.duration).unwrap(),
                    );

                    // Increase the number of calls every time you get a hit of overlaps
                    if overlap {
                        c_result[s_position].number_of_calls =
                            c_result[s_position].number_of_calls + 1;
                        println!("s_position: {}", s_position);
                    }
                }
            }

            // Sort the vector by the number of calls
            c_result.sort_by(|a, b| b.number_of_calls.cmp(&a.number_of_calls));

            // Print every result but only the first 4
            for (s, c) in c_result.iter().enumerate() {
                if s < 4 {
                    println!("result: {}", c);
                }
            }
        }

        Err(err) => panic!("error reading CSV Data: {:?}", err),
    };
}
