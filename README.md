# Parallel Calls

This is a little program written in Rust, which finds
parallel calls in a .csv file.

## Idea

The foundation is structured like this:

```
06.09.2021 12:32:44, 00:00:43
06.09.2021 12:35:48, 00:00:30
06.09.2021 12:32:44, 00:00:43
```

So there is always a timestamp and a duration.

The program detects overlappings of such calls,
and counts how many of them are simultaniously.

```
result: First Appearence: 2022-01-19 11:00:42, Number of calls: 21
result: First Appearence: 2022-01-18 13:25:51, Number of calls: 13
result: First Appearence: 2022-01-18 13:12:39, Number of calls: 10
result: First Appearence: 2022-01-20 15:42:37, Number of calls: 9
```

## Dependencies

The dependencies are an installation of Rust 2021 Edition,
but earlier versions should work fine.

The crates beeing used are:

- csv for reading the data
- serde for deserializing the csv into time/duration
- chrono for dealing with time stuff

Do `cargo build release` for building from source

or debug with `cargo run < data.csv`

## Usage

Using is quite simple
```
rst_parallel_calls < data.csv
```

## Competition

The program was part of a competition from [gnulinux.ch](https://gnulinux.ch/wettbewerb-parallele-anrufe)

It is out of competition since it's not written in Shell/Python.
I simply wanted to learn rust.

## Todo

- This is a very naive approach, and not done the "Rust way" at all. Improve on that!
- Use clap for a proper CLI
- There is no real feedback, maybe a progressbar would be nice
